FROM ubuntu:18.04
LABEL maintainer="kwonghung.yip@gmail.com"

ENV SDKMAN_DIR="/usr/local/sdkman"

SHELL ["/bin/bash","-c"]
RUN apt-get update && apt-get install -y curl zip unzip 

RUN curl -s "https://get.sdkman.io" | bash
#RUN /bin/bash -c "source ${SDKMAN_DIR}/bin/sdkman-init.sh && sdk version"
#RUN source ${SDKMAN_DIR}/bin/sdkman-init.sh && sdk version

#Change the sdkman setting to enable installation in slient mode
RUN sed -i -e 's/sdkman_auto_answer=false/sdkman_auto_answer=true/' ${SDKMAN_DIR}/etc/config

CMD ["bash"]
